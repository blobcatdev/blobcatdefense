require "code/grid_tile_types"

GridTile = {
    NULL = {},
    OBSCURED = {},
    GRASS = {
        type = GridTileType.SLOT,
        c = {0,154,23}
    },
    DIRT = {
        type = GridTileType.PATH,
        c = {155,118,83}
    },
    RIVER = {
        type = GridTileType.WATER,
        c = {175,207,234}
    },
    LAKE = {
        type = GridTileType.WATER,
        c = {175,178,234}
    },
    SAND = {
        type = GridTileType.SLOT,
        c = {239,239,158}
    }
}

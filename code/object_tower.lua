require "code/object_tower_types"
require "code/object_tower_units"

Tower = class(Object)

function Tower:new(data)
    local _o = Object:new()

    _o.t = 0
	_o.type = data.type

    _o.texture = data.texture

    _o.max_health = data.max_health
    _o.health = _o.max_health
    _o.armor = data.armor

    _o.attack_damage = data.attack_damage
    _o.attack_speed = data.attack_speed 
    _o.range = data.range

    _o.bounce = 0
    
	setmetatable(_o, self)
	self.__index = self
	return _o
end

function Tower:step(dt)
    self.t = self.t + dt
end

function Tower:getGridPixelX(x)
    return GridData.tile_size[1]*(x-1)
end

function Tower:getGridPixelY(y)
    return GridData.tile_size[1]*(y-1)
end

function Tower:getBounce(_a, _w)
    _a = _a/2
    _a = _a/(GridData.tile_size[1]) * (GridData.tile_size[1])/self.texture:getWidth()
    return _a * math.sin(_w*self.t)
end

function Tower:draw(_i,_j) 
    local _offset_x = _offset_x or GridData.offset[1]
    local _offset_y = _offset_y or GridData.offset[2]
    local _grid_x = self:getGridPixelX(_j)
    local _grid_y = self:getGridPixelY(_i)
    local _bounce = self:getBounce(8, 3)

    if self.camp then
        _bounce = self:getBounce(8, 1.5)
    end
    love.graphics.print(_bounce*(GridData.tile_size[1]))
    love.graphics.draw(
        self.texture,
        _offset_x + _grid_x - _bounce*(GridData.tile_size[1]),
        _offset_y + _grid_y - _bounce*(GridData.tile_size[2]) *1.9,
        0,
        (GridData.tile_size[1])/self.texture:getWidth()  + _bounce,
        (GridData.tile_size[2])/self.texture:getHeight() + _bounce
    )
    self:drawExtras(_i,_j) 
end

function Tower:turnIntoCamp(_i,_j)
    self.texture = getRandomTableElement(img.camp)
    self.camp = true

    if self.texture == img.camp["straw1"]
    or self.texture == img.camp["straw2"]
    or self.texture == img.camp["straw3"] 
    or self.texture == img.camp["_police"] 
    or self.texture == img.camp["santa_xmas"] then
        self:turnIntoCamp(_i,_j)
    elseif self.texture == img.camp["snug1"] then
        if Grid[_i][_j+1] and Grid[_i][_j+1].tower and Grid[_i][_j+1].tower.texture ~= img.camp["snug2"] then 
            Grid[_i][_j+1].tower.texture = img.camp["snug2"]
        else
            self:turnIntoCamp(_i,_j)
        end
    elseif self.texture == img.camp["snug2"] then
        if Grid[_i][_j-1] and Grid[_i][_j-1].tower and Grid[_i][_j-1].tower.texture ~= img.camp["snug1"] then 
            Grid[_i][_j-1].tower.texture = img.camp["snug1"]
        else
            self:turnIntoCamp(_i,_j)
        end
    elseif self.texture == img.camp["hug1"] then
        if Grid[_i][_j+1] and Grid[_i][_j+1].tower and Grid[_i][_j+1].tower.texture ~= img.camp["hug2"] then 
            Grid[_i][_j+1].tower.texture = img.camp["hug2"]
        else
            self:turnIntoCamp(_i,_j)
        end
    elseif self.texture == img.camp["hug2"] then
        if Grid[_i][_j-1] and Grid[_i][_j-1].tower and Grid[_i][_j-1].tower.texture ~= img.camp["hug1"] then 
            Grid[_i][_j-1].tower.texture = img.camp["hug1"]
        else
            self:turnIntoCamp(_i,_j)
        end
    elseif self.texture == img.camp["drink"] then 
        if not Grid[_i][_j+1] or Grid[_i][_j+1].tower then 
            self:turnIntoCamp(_i,_j)
        end
    end
end
function Tower:drawExtras(_i,_j)
    local _offset_x = _offset_x or GridData.offset[1]
    local _offset_y = _offset_y or GridData.offset[2]
    local _grid_x = self:getGridPixelX(_j)
    local _grid_y = self:getGridPixelY(_i)
    local _bounce = self:getBounce(4, 3)

    if self.texture == img.camp["drink"] then 
        local _grid_x = self:getGridPixelX(_j+1)
        self.drink = self.drink or math.random(3)
        love.graphics.draw(
            img.camp["straw"..self.drink],
            _offset_x + _grid_x + 1 * _bounce * GridData.tile_size[1],
            _offset_y + _grid_y - 2 * _bounce * GridData.tile_size[2] - GridData.tile_size[2]*0.1,
            0,
            (GridData.tile_size[1])/self.texture:getWidth()  - _bounce,
            (GridData.tile_size[2])/self.texture:getHeight() + _bounce
        )
    end
end
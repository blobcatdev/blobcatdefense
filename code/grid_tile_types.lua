GridTileType = enumTable {
    "PATH",
    "SLOT",
    "WATER",
    "OCCUPIED"
}

function getValidTilesTypes(_towertype)
    local _v = {}

    if _towertype == TowerType.MELEE_BLOCK
    or _towertype == TowerType.MELEE_GHOST then 
        table.insert(_v,GridTileType.PATH)
    elseif _towertype == TowerType.RANGED then
        table.insert(_v,GridTileType.SLOT)
    elseif _towertype == TowerType.WATER then
        table.insert(_v,GridTileType.WATER)
    else
        for k in pairs(GridTileType) do
            table.insert(_v,GridTileType[k])
        end
    end
    return _v
end
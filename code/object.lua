ObjectHolder = {}
Object = class()

function Object:new(data)
    _o = {}
	
	setmetatable(_o, self)
	self.__index = self
	return _o
end

function Object:remove() 
	self.remove = true
end

function stepObjects(dt)
	for i=1, #ObjectHolder do
		local _o = ObjectHolder[i]
		_o:step(dt)
	end
end

function cleanObjects()
	for i=1, #ObjectHolder do
		local _o = ObjectHolder[i]
        if _o and _o.remove then
			table.remove(ObjectHolder,i)
		end
	end
end

require "code/object_tower"
require "code/object_enemy"
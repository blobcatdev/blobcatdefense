TowerUnit = {
    Blobcat = {
        texture = love.graphics.newImage("textures/object_tower_units/blobcat.png"),
        type = TowerType.RANGED,
        max_health = 4,
        armor = 0,
        attack_damage = 2,
        attack_speed = 1,
        range = 1,
        desc = "your everyday, average blobcat",
        hint = ""
    },
    Healer = {
        texture = love.graphics.newImage("textures/object_tower_units/heal.png"),
        type = TowerType.RANGED,
        max_health = 6,
        armor = 0,
        attack_damage = -3,
        attack_speed = 0.25,
        range = 3,
        desc = "heal your poor wounded frens :(",
        hint = "heals your friends for 3 HP every 4 seconds"
    },
    Artist = {
        texture = love.graphics.newImage("textures/object_tower_units/artist.png"),
        type = TowerType.RANGED,
        max_health = 5,
        armor = 2,
        attack_damage = 0,
        attack_speed = 0.5,
        range = 1,
        desc = "a makeup artist, turning foxxos into blobbos..",
        hint = "turns foxes into masked cats, causing other foxes to attack them!"
    },
    Box = {
        texture = love.graphics.newImage("textures/object_tower_units/box.png"),
        type = TowerType.MELEE_BLOCK,
        max_health = 20,
        armor = 0,
        attack_damage = 9999999, --its funny cause it will never attack
        attack_speed = 0,
        range = 0,
        desc = "cat in a box. it aint leaving the box anytime soon",
        hint = "takes a bunch of damage for you, and then turns back into a normal blobcat"
    },
    Happy = {
        texture = love.graphics.newImage("textures/object_tower_units/happy.png"),
        type = TowerType.RANGED,
        max_health = 5,
        armor = 0,
        attack_damage = 0,
        attack_speed = 0.3,
        range = 4, --effect: speed++ to frens
        desc = "moral support for all~",
        hint = "gives a attack speed boost to all friends in range"
    },
    Sad = {
        texture = love.graphics.newImage("textures/object_tower_units/sad.png"),
        type = TowerType.RANGED,
        max_health = 7,
        armor = 0,
        attack_damage = 1,
        attack_speed = 0.8,
        range = 1,
        desc = "who hurt this poor thing :(",
        hint = "demoralizes enemies, bringing their attack speed down"
    },
    Sadist = {
        texture = love.graphics.newImage("textures/object_tower_units/flushed.png"),
        type = TowerType.RANGED,
        max_health = 8,
        armor = 2,
        attack_damage = 5,
        attack_speed = 0.5,
        range = 5,
        desc = "turned on by hurting others...",
        hint = "" -- can we specify how projectiles look? cuz if so. LAZER BEAMS YOOOOO
    },
    Baguette = {
        texture = love.graphics.newImage("textures/object_tower_units/baguette.png"),
        type = TowerType.MELEE_GHOST,
        max_health = 4,
        armor = 4,
        attack_damage = 3,
        attack_speed = 1.5,
        range = 1,
        desc = "this one is hyped to slay some foxxos~",
        hint = ""
    }
}
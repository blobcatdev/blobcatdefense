require "code/grid_maze"
require "code/grid_tile"

GridData = { 
    tile_size = {48,48},
    offset = {0,0},
    tower_padding = 2
}

function blankGrid(w,h)
    Grid = {}
    for i=1, h do
        Grid[i] = {}
        for j=1, w do
            Grid[i][j] = {
                tile = GridTile.NULL,
                tower = nil
            }
        end
    end
    generateMaze()
    centerGrid()
end

function centerGrid()
    local _height = #Grid
    local _width = #Grid[1]

    GridData.offset[1] = (game_width - _width*GridData.tile_size[1])/2
    GridData.offset[2] = (game_height - _height*GridData.tile_size[2])/2
end

function stepGrid(dt)
    for i=1, #Grid do
        for j=1, #Grid[i] do
            if Grid[i][j].tower then 
                Grid[i][j].tower:step(dt) 
            end
        end
    end   
end

function drawGrid()
    local _height = #Grid
    local _width = #Grid[1]

    -- draw fake grid for now, this should be replaced with actually generated grids lol
    -- or not, only when level is finished
    for i=-1, 1 do 
        for j=-1, 1 do 
            for x=1, 8 do
                for y=1, 8 do
                    drawTile(
                        GridTile.OBSCURED,
                        x,
                        y,
                        GridData.offset[1]+(_width*GridData.tile_size[1]+3) * i,
                        GridData.offset[2]+(_height*GridData.tile_size[2]+3) * j
                    )
                end
            end
        end
    end

    for i=1, #Grid do
        for j=1, #Grid[i] do
            if Grid[i][j].tile then
                drawTile(
                    Grid[i][j].tile,
                    i,
                    j,
                    nil,
                    nil,
                    Grid[i][j].highlight,
                    Grid[i][j].entrance or Grid[i][j].exit
                )
            end
        end
    end

    for i=1, #Grid do
        for j=1, #Grid[i] do
            if Grid[i][j].tower then
               Grid[i][j].tower:draw(j,i)
            end
        end
    end
end

function drawTile(_tile, _x, _y, _offset_x, _offset_y, _highlight, _arrow)
    if _tile ~= GridTile.NULL then
        local _c1 = 0.1
        local _c2 = 0.1
        local _c3 = 0.1

        local _offset_x = _offset_x or GridData.offset[1]
        local _offset_y = _offset_y or GridData.offset[2]
        
        if _tile ~= GridTile.OBSCURED then 
            _c1 = _tile.c[1]/255
            _c2 = _tile.c[2]/255
            _c3 = _tile.c[3]/255

            if (_x + _y) % 2 == 0 then
                _c1 = _c1 * 0.88
                _c2 = _c2 * 0.88
                _c3 = _c3 * 0.88
            end
        end 

        if _highlight then
            love.graphics.setColor(
                (_c1 + _highlight[1]) / 2,
                (_c2 + _highlight[2]) / 2,
                (_c3 + _highlight[3]) / 2
            )
        else
            love.graphics.setColor(
                _c1,
                _c2,
                _c3
            )
        end

        love.graphics.rectangle(
            "fill",
            _offset_x + GridData.tile_size[1]*(_x-1),
            _offset_y + GridData.tile_size[2]*(_y-1),
            GridData.tile_size[1],
            GridData.tile_size[2]
        )
        
        if _highlight then
            love.graphics.setColor(
                0.8,
                0.8,
                0.8
            )
        else
            love.graphics.setColor(1,1,1)
        end

        if _arrow then 
            love.graphics.draw(
                img.arrow,
                _offset_x + GridData.tile_size[1]*(_x-1),
                _offset_y + GridData.tile_size[2]*(_y-1),
                0,
                GridData.tile_size[1]/img.arrow:getWidth(),
                GridData.tile_size[2]/img.arrow:getHeight()
            )
        end

        love.graphics.setColor(1,1,1)
    end
end

function getGridInCanvas(_x,_y)
    local _x = math.floor((_x - GridData.offset[1])/GridData.tile_size[1])+1
    local _y = math.floor((_y - GridData.offset[2])/GridData.tile_size[2])+1

    if  _x > 0 and _x <= #Grid[1]
    and _y > 0 and _y <= #Grid then
        return Grid[_x][_y]
    else
        return nil 
    end
end
MazeType = enumTable {
    STRAIGHT
    --SERPENTINE,
    --TURN,
    --SERPENTINE_TURN
}

MazeSide = enumTable {
    TOP,
    RIGHT,
    BOTTOM,
    LEFT
}

MazeSideCoords = {
    {1,0},
    {0,8},
    {8,0},
    {0,1}
}

MazeSidePathVector = {
    {1,0},
    {0,-1},
    {-1,0},
    {0,1}
}
function getMapGrid() -- returns 8x8 of tile types
    --tmp!!!!!!!!!
    for i=1, 8 do
        Grid[i] = {}
        for j=1, 8 do
            Grid[i][j] = {
                tile = GridTile.GRASS,
                tower = nil
            }
        end
    end
end

function coordsToSide(_coords)
    for i=1, #MazeSideCoords do -- if any of the coords matches a side, then :)
        if _coords[1] == MazeSideCoords[i][1]
        or _coords[2] == MazeSideCoords[i][2] then 
            return i
        end
    end
end

function getMazeExit(_in) -- returns i and j of map exit
    local _inside = coordsToSide(_in)
    local _outside = 0

    if _inside == MazeSide.BOTTOM then
        _outside = getRandomTableElement(
            {
                MazeSide.LEFT, 
                MazeSide.TOP, 
                MazeSide.RIGHT
            }
        )
    elseif _inside == MazeSide.LEFT then
        _outside = getRandomTableElement(
            {
                MazeSide.BOTTOM, 
                MazeSide.TOP, 
                MazeSide.RIGHT
            }
        )
    elseif _inside == MazeSide.TOP then
        _outside = getRandomTableElement(
            {
                MazeSide.BOTTOM, 
                MazeSide.LEFT, 
                MazeSide.RIGHT
            }
        )
    elseif _inside == MazeSide.RIGHT then
        _outside = getRandomTableElement(
            {
                MazeSide.BOTTOM, 
                MazeSide.LEFT, 
                MazeSide.TOP
            }
        )
    end

    local _coords = {1,0}
    for i=1, #_coords do
        if _coords[i] == 0 then
            _coords[i] = math.random(6)+1
        end
    end
    return _coords
end

function getStrongAxis(_vector)
    for i=1, #_vector do
        if _vector[i] == 1 or _vector[i] == 8 then 
            return i
        end
    end
end

function getWeakAxis(_vector)
    for i=1, #_vector do
        if i ~= getStrongAxis(_vector) then
            return i
        end
    end
end

function generateMaze(_in, _branches) 
    math.randomseed(os.time())
    local _type = MazeType.STRAIGHT --temporary for dev-- getRandomTableElement(MazeType)

    local _in = _in or {8,math.random(6)+1}
    local _out = getMazeExit(_in)

    getMapGrid()  -- returns 8x8 of tile types

    local _inside = coordsToSide(_in)
    local _outside = coordsToSide(_out) -- <- here
    local _invector = MazeSidePathVector[_inside]
    local _outvector = MazeSidePathVector[_outside]
    
    if _type == MazeType.STRAIGHT then
        local _current = {_in[1],_in[2]}

        local _rng = math.random(6)

        print("in" ,_in[1], _in[2])
        print("out", _out[1], _out[2])
        local _sidesteps = _out[getWeakAxis(_out)] - _in[getWeakAxis(_in)]
        print("sidesteps", _sidesteps)
        print("turn at", _rng)

        -- place entrance
        Grid[_current[1]][_current[2]].tile = GridTile.DIRT

        -- place blocks in forward direction for that many
        for _x = 1, _rng do
            _current[1] = _current[1] + _invector[1]
            _current[2] = _current[2] + _invector[2]
            print("_current", _current[1], _current[2])
            Grid[_current[1]][_current[2]].tile = GridTile.DIRT
        end
        
        
        for i = 1, math.abs(_sidesteps) do 
            _current[getWeakAxis(_in)] = _current[getWeakAxis(_in)] + math_sign(_sidesteps)
            print("_current", _current[1], _current[2])
            Grid[_current[1]][_current[2]].tile = GridTile.DIRT
        end

        for _x = 1, 6-_rng do
            _current[1] = _current[1] + _invector[1]
            _current[2] = _current[2] + _invector[2]
            print("_current", _current[1], _current[2])
            Grid[_current[1]][_current[2]].tile = GridTile.DIRT
        end
        _current = _out
        Grid[_current[1]][_current[2]].tile = GridTile.DIRT
    elseif _type == MazeType.TURN then
        
    elseif _type == MazeType.SERPENTINE_STRAIGHT then

    end
end
    -- function takes a few args
    -- in side, out side(s), and also  number of branches

    -- and then just . tosses back a 8x8 array




-- what if , 
-- it . starts by just generating a path to and from the. (wall)

-- in a straight-ish line (i mean if its from bottom to right wall. good luck having it straight)

-- and then modifying it to have a bunch of turns n such
-- true but .     it would make for more interesting gameplay
require "code/object_enemy_types"
require "code/object_enemy_units"

Enemy = class(Object)

function Enemy:new(data)
    local _o = Object:new()

	_o.type = data.type
    _o.texture = data.texture

    _o.max_health = data.max_health
    _o.health = _o.max_health
    _o.armor = data.armor

    _o.attack_damage = data.attack_damage
    _o.attack_speed = data.attack_speed 
    _o.range = data.range

	setmetatable(_o, self)
	self.__index = self
	return _o
end
img = {}
img.arrow = love.graphics.newImage("textures/tiles/tile_triangle.png")
debug_images = ""
img.camp = {}
for _, image_path in ipairs(love.filesystem.getDirectoryItems("textures/object_camp")) do
    img.camp[string.gsub(image_path,".png","")] = love.graphics.newImage("textures/object_camp/" .. image_path)
end
function getRandomTableElement(_tb)
    local _keys = {}
    for _k in pairs(_tb) do 
        table.insert(_keys, _k)
    end
    return _tb[_keys[math.random(#_keys)]]
end

function enumTable(tbl)
	for i = 1, #tbl do
		local v = tbl[i]
		tbl[v] = i
	end
	return tbl
end
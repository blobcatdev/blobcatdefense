require "code/grid"
require "code/object"

TowerDeck = {
    Remove = {
        texture = love.graphics.newImage("textures/object_tower_units/red_cross.png"),
        type = TowerType.SPECIAL
    }
}

for k, v in pairs(TowerUnit) do 
    TowerDeck[k] = v
end

TowerDeckSelect = 0
TowerDeckSelectedUnit = nil

function stepDeck()
    if love.mouse.isDown(2) then
        for i=1, #Grid do
            for j=1, #Grid[i] do
                if Grid[i][j].tower and not Grid[i][j].tower.camp then 
                    Grid[i][j].tower:turnIntoCamp(i,j)
                end
            end
        end
    end


    for i=1, #Grid do
        for j=1, #Grid[i] do
            Grid[i][j].highlight = nil
        end
    end
    
    TowerDeckSelect = 0
    
    local _size = {64,64}
    local _title_space = 11
    local _sep = 3
    local _width = #TowerDeck*_size[1] + _sep*(#TowerDeck-1)
    local _x = _width + _title_space
    local _y = game_height -_size[2] - _sep*2 - _title_space
    local _mouse_x = love.mouse.getX()
    local _mouse_y = love.mouse.getY()

    if TowerDeckSelectedUnit then
        TowerDeckSelect = 0
        for i=1, #Grid do
            for j=1, #Grid[i] do
                if TowerDeckSelectedUnit == TowerUnit.Remove then
                    if Grid[i][j].tower then 
                        Grid[i][j].highlight = {0.0,0.0,0.0}
                    end
                else
                    for _, _type in pairs(getValidTilesTypes(TowerDeckSelectedUnit.type)) do
                        if Grid[i][j].tile.type ~= _type then
                            Grid[i][j].highlight = {0.2,0.2,0.2}
                        end 
                    end
                end
            end
        end
        
        if love.mouse.isDown(1) then
            local _grid_tile = getGridInCanvas(_mouse_x,_mouse_y)

            if _grid_tile then
                for _, _valid_tile in pairs(getValidTilesTypes(TowerDeckSelectedUnit.type)) do 
                    if TowerDeckSelectedUnit == TowerDeck.Remove then 
                        _grid_tile.tower = nil
                    elseif _grid_tile.tile.type == _valid_tile then
                        if not _grid_tile.tower then
                           _grid_tile.tower = Tower:new(TowerDeckSelectedUnit)
                        end
                    end 
                end
            end
               
            TowerDeckSelectedUnit = nil
        elseif love.mouse.isDown(2) then
            TowerDeckSelectedUnit = nil
        end
    end

    local _i = 0
    for _unit in pairs(TowerDeck) do
        local _pos_x = _x + _i*_size[1] + _sep*(_i-1)
        local _pos_y = _y

        _i = _i + 1
        if  _mouse_x < _pos_x + _size[1] 
        and _mouse_x > _pos_x 
        and _mouse_y < _pos_y + _size[2] 
        and _mouse_y > _pos_y then
            TowerDeckSelect = _i
            if love.mouse.isDown(1) then
                TowerDeckSelectedUnit = TowerDeck[_unit]
            end
        end
    end
end

function drawDeck()
    local _size = {64,64}
    local _title_space = 11
    local _sep = 3
    local _width = #TowerDeck*_size[1] + _sep*(#TowerDeck-1)
    local _x = _width + _title_space
    local _y = game_height -_size[2] - _sep*2 - _title_space

    local _i = 0
    for _unit, _stats in pairs(TowerDeck) do
        local _name = love.graphics.newText( game_font, "" )
        _name:setf(_unit,_size[1],"center")
        if TowerDeckSelect == _i+1 or _stats == TowerDeckSelectedUnit then 
            _name:setf({{1,1,0},_unit},_size[1],"center")
            love.graphics.rectangle(
                "line", 
                _x + _i*_size[1] + _sep*(_i-1),
                _y,
                _size[1],
                _size[2]
            )
        elseif TowerDeckSelectedUnit then
            _name:setf({{0.3,0.3,0.3},_unit},_size[1],"center")
        end
        love.graphics.draw(
            _name,
            math.floor(_x + _i*_size[1] + _sep*(_i-1)+0.5),
            math.floor(_y + _size[2] +0.5)

        )
        love.graphics.draw(
            _stats.texture,
            math.floor(_x + _i*_size[1] + _sep*(_i-1)+0.5), 
            math.floor(_y+0.5),
            0,
            _size[1]/_stats.texture:getWidth(),
            _size[2]/_stats.texture:getHeight()
        )
        _i = _i + 1
    end
    
    if TowerDeckSelectedUnit then 
        local _mouse_x = love.mouse.getX()
        local _mouse_y = love.mouse.getY()
        local _size = {32,32}

        love.graphics.draw(
            TowerDeckSelectedUnit.texture,
            math.floor(_mouse_x-GridData.tile_size[1]/2),
            math.floor(_mouse_y-GridData.tile_size[2]/2),
            0,
            _size[1]/TowerDeckSelectedUnit.texture:getWidth(),
            _size[2]/TowerDeckSelectedUnit.texture:getHeight()
        )
    
    end
end

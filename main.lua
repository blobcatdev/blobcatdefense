
-- This function will return a string filetree of all files
-- in the folder and files in all subfolders
function recursiveEnumerate(folder, fileTree)
    local filesTable = love.filesystem.getDirectoryItems(folder)
    for i,v in ipairs(filesTable) do
        local file = folder.."/"..v
        local info = love.filesystem.getInfo(file)
        if info then
            if info.type == "file" then
                fileTree = fileTree.."\n"..file
            elseif info.type == "directory" then
                fileTree = fileTree.."\n"..file.." (DIR)"
                fileTree = recursiveEnumerate(file, fileTree)
            end
        end
    end
    return fileTree
end

function love.load()

    game_width = 640
    game_height = 640
    game_font = love.graphics.getFont()

    require "img"
    require "tables"
    require "class"
    require "game"
    require "menu"
    require "_math"


    blankGrid(8,8)
end

function love.update(dt)
    stepGrid(dt)
    stepDeck()
    cleanObjects()
end

function love.draw()
    drawGrid()
    drawDeck()
    love.graphics.print(debug_images)
end 

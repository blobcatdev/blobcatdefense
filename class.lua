function class(super, self)
	assert(super == nil or super.__index == super)
	self = self or {}
	self.__index = self
	setmetatable(self, super)
	return self
end

function getAncestors(self)
	local family = self
	local list = {}
	while family ~= nil do
		family = getmetatable(family)
		table.insert(list,family)
	end
	return list
end
